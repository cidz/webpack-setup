tutorial from https://www.valentinog.com/blog/from-gulp-to-webpack-4-tutorial/
tutorial from https://www.valentinog.com/blog/webpack-tutorial/

# Description

This is a webpack setup template with :
compiling SASS to CSS,
webpack-dev-server,
set up Babel ,
image compression, 
watching files and re-compiling on save and
HTML webpack plugin

# Dependencies

``` bash
# NPM
npm install

```